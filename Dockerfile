# Defines the base image layer
FROM python:3.10.0a7-slim-buster

# Prevents Python from writing out pyc files
ENV PYTHONDONTWRITEBYTECODE 1

# Keeps Python from buffering stdin/stdout
ENV PYTHONUNBUFFERED 1

# Creates virtual environment
ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# Installs dependencies and creates non-root user
COPY requirements.txt .
RUN python3 -m pip install --upgrade pip &&\ 
    pip install -r requirements.txt &&\ 
    groupadd -g 1001 gallerygroup &&\ 
    useradd -ms /bin/bash -u 1001 -g 1001 galleryrun
USER galleryrun
WORKDIR /home/galleryrun/app

# Files for the application
COPY --chown=galleryrun:gallerygroup ./gallery-app .

# The container listens on the 8080 network port at runtime
EXPOSE 8080

# Run server application
CMD ["python3", "manage.py", "runserver", "0.0.0.0:8080"]