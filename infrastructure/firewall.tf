resource "google_compute_firewall" "ingress_firewall" {
  name    = "${var.cluster}-ingress-firewall-rule"
  network = google_compute_network.network.name

  depends_on = [
    google_container_node_pool.preemptible_nodes
  ]

  allow {
    protocol = var.firewall_protocol
    ports    = var.firewall_ports
  }

  source_ranges = [google_container_cluster.primary.private_cluster_config.0.master_ipv4_cidr_block]
  target_tags   = google_container_node_pool.preemptible_nodes.node_config.0.tags
}
