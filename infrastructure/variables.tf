variable "project" {
  type        = string
  default     = "gallery-app-project-350311"
  description = "project ID"
}

variable "region" {
  type        = string
  default     = "us-central1"
  description = "name of the region"
}

variable "network" {
  type        = string
  default     = "gallery-app-network"
  description = "name of the network being created"
}

variable "subnetwork_range" {
  type        = string
  default     = "10.40.0.0/16"
  description = "CIDR for subnetwork nodes"
}

variable "subnetwork_pods" {
  type        = string
  default     = "10.41.0.0/16"
  description = "secondary CIDR for pods"
}

variable "subnetwork_services" {
  type        = string
  default     = "10.42.0.0/16"
  description = "secondary CIDR for services"
}

variable "nat_allocation" {
  type        = string
  default     = "MANUAL_ONLY"
  description = "how external IPs should be allocated for the NAT"
}

variable "nat_subnetworks" {
  type        = string
  default     = "LIST_OF_SUBNETWORKS"
  description = "how NAT should be configured per subnetwork"
}

variable "nat_ipranges" {
  type        = string
  default     = "ALL_IP_RANGES"
  description = "list of options for which source IPs in the subnetwork should have NAT enabled"
}

variable "nat_address_type" {
  type        = string
  default     = "EXTERNAL"
  description = "type of address to reserve"
}

variable "nat_network_tier" {
  type        = string
  default     = "PREMIUM"
  description = "networking tier used for configuring this address"
}

variable "cluster" {
  type        = string
  default     = "gallery-app-primary-cluster"
  description = "name of the primary cluster"
}

variable "master_cidr" {
  type        = string
  default     = "172.16.0.0/28"
  description = "CIDR for master ipv4 block"
}

variable "networking_mode" {
  type        = string
  default     = "VPC_NATIVE"
  description = "networking mode of the primary cluster"
}

variable "node_count" {
  type        = number
  default     = 1
  description = "node count"
}

variable "machine_type" {
  type        = string
  default     = "e2-small"
  description = "name of the machine type used for the cluster nodes"
}

variable "firewall_protocol" {
  type        = string
  default     = "tcp"
  description = "IP protocol to which this rule applies"
}

variable "firewall_ports" {
  type        = list(string)
  default     = ["8443"]
  description = "ports to which this rule applies"
}

variable "flux_target_path" {
  type        = string
  default     = "kubernetes/"
  description = "target path to the kubernetes resources"
}

variable "flux_repository" {
  type        = string
  default     = "ssh://git@gitlab.com/cyanopus/continuous-delivery-with-gitops-in-kubernetes.git"
  description = "git url to the kubernetes resources"
}

variable "flux_namespace" {
  type        = string
  default     = "flux-system"
  description = "The flux namespace."
}

variable "flux_ssh_pub_key" {
  sensitive   = true
  type        = string
  description = "public key for flux"
}

variable "flux_ssh_priv_key" {
  sensitive   = true
  type        = string
  description = "private key for flux"
}

variable "app_namespace" {
  type        = string
  default     = "gallery-app"
  description = "namespace where the app will be deployed"
}

variable "registry_secret_name" {
  type        = string
  default     = "registry-credentials"
  description = "name of the secret for the container gitlab registry"
}

variable "registry_server" {
  type        = string
  default     = "https://registry.gitlab.com"
  description = "link to the registry server"
}

variable "registry_username" {
  sensitive   = true
  type        = string
  description = "username to access the registry"
}

variable "registry_password" {
  sensitive   = true
  type        = string
  description = "password to access the registry"
}
