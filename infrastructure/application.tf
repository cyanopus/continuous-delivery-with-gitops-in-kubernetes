resource "kubernetes_namespace" "gallery_app" {
  metadata {
    name = var.app_namespace

    labels = {
      mylabel = var.app_namespace
    }
  }

  depends_on = [
    google_container_node_pool.preemptible_nodes
  ]
}

resource "kubernetes_secret" "registry" {
  metadata {
    name      = var.registry_secret_name
    namespace = kubernetes_namespace.gallery_app.metadata.0.name
  }

  data = {
    ".dockerconfigjson" = jsonencode({
      auths = {
        "${var.registry_server}" = {
          auth = "${base64encode("${var.registry_username}:${var.registry_password}")}"
        }
      }
    })
  }

  type = "kubernetes.io/dockerconfigjson"

  depends_on = [
    kubernetes_namespace.gallery_app
  ]
}
