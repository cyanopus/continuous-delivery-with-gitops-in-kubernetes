data "flux_install" "main" {
  target_path = var.flux_target_path
}

data "flux_sync" "main" {
  target_path = var.flux_target_path
  url         = var.flux_repository
}

data "kubectl_file_documents" "apply" {
  content = data.flux_install.main.content
}

data "kubectl_file_documents" "sync" {
  content = data.flux_sync.main.content
}

locals {
  apply = [for v in data.kubectl_file_documents.apply.documents : {
    data : yamldecode(v)
    content : v
    }
  ]

  sync = [for v in data.kubectl_file_documents.sync.documents : {
    data : yamldecode(v)
    content : v
    }
  ]

  known_hosts = "gitlab.com ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY="
}

resource "kubernetes_namespace" "flux_system" {
  metadata {
    name = var.flux_namespace
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }

  depends_on = [
    google_container_node_pool.preemptible_nodes
  ]
}

resource "kubectl_manifest" "apply" {
  for_each = {
    for v in local.apply : lower(join("/",
      compact([v.data.apiVersion, v.data.kind,
        lookup(v.data.metadata, "namespace", ""),
    v.data.metadata.name]))) => v.content
  }

  depends_on = [
    kubernetes_namespace.flux_system
  ]

  yaml_body = each.value
}

resource "kubectl_manifest" "sync" {
  for_each = {
    for v in local.sync : lower(join("/",
      compact([v.data.apiVersion, v.data.kind,
        lookup(v.data.metadata, "namespace", ""),
    v.data.metadata.name]))) => v.content
  }

  depends_on = [
    kubernetes_namespace.flux_system
  ]
  yaml_body = each.value
}

resource "kubernetes_secret" "main" {
  depends_on = [
    kubectl_manifest.apply
  ]

  metadata {
    name      = data.flux_sync.main.secret
    namespace = data.flux_sync.main.namespace
  }

  data = {
    identity       = var.flux_ssh_priv_key
    "identity.pub" = var.flux_ssh_pub_key
    known_hosts    = local.known_hosts
  }
}

