resource "google_compute_network" "network" {
  project                 = var.project
  name                    = var.network
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "subnetwork" {
  name                     = "${var.network}-subnet"
  region                   = var.region
  network                  = google_compute_network.network.self_link
  ip_cidr_range            = var.subnetwork_range
  private_ip_google_access = true

  secondary_ip_range {
    range_name    = "${var.network}-pods"
    ip_cidr_range = var.subnetwork_pods
  }

  secondary_ip_range {
    range_name    = "${var.network}-services"
    ip_cidr_range = var.subnetwork_services
  }
}

resource "google_compute_router" "router" {
  name    = "${var.network}-router"
  region  = var.region
  network = google_compute_network.network.self_link
}

resource "google_compute_address" "nat_address" {
  name         = "${var.network}-address"
  address_type = var.nat_address_type
  network_tier = var.nat_network_tier
}

resource "google_compute_router_nat" "nat" {
  name                               = "${var.network}-nat"
  router                             = google_compute_router.router.name
  region                             = var.region
  nat_ip_allocate_option             = var.nat_allocation
  source_subnetwork_ip_ranges_to_nat = var.nat_subnetworks

  subnetwork {
    name                    = google_compute_subnetwork.subnetwork.self_link
    source_ip_ranges_to_nat = [var.nat_ipranges]
  }

  nat_ips = [google_compute_address.nat_address.self_link]
}
