resource "google_container_cluster" "primary" {
  project    = var.project
  location   = var.region
  name       = var.cluster
  network    = google_compute_network.network.self_link
  subnetwork = google_compute_subnetwork.subnetwork.self_link

  ip_allocation_policy {
    cluster_secondary_range_name  = google_compute_subnetwork.subnetwork.secondary_ip_range.0.range_name
    services_secondary_range_name = google_compute_subnetwork.subnetwork.secondary_ip_range.1.range_name
  }

  private_cluster_config {
    enable_private_nodes    = true
    enable_private_endpoint = false
    master_ipv4_cidr_block  = var.master_cidr
  }

  initial_node_count       = 1
  remove_default_node_pool = true
  networking_mode          = var.networking_mode
}

resource "google_container_node_pool" "preemptible_nodes" {
  name       = "${var.cluster}-node-pool"
  location   = var.region
  cluster    = google_container_cluster.primary.name
  node_count = var.node_count

  node_config {
    preemptible  = true
    machine_type = var.machine_type

    metadata = {
      disable-legacy-endpoints = true
    }

    tags = ["${var.cluster}-node"]
  }
}
